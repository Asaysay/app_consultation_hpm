import 'package:flutter/material.dart';
import 'package:flutter_posts/pages/AppBarr.dart';
import 'package:flutter_posts/pages/entrees.dart';
import 'package:flutter_posts/pages/post.dart';

import 'pages/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      // remove debug banner
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),


      onGenerateRoute: (settings){
        if(settings.name == '/'){
          return MaterialPageRoute(builder: (context) => HomePage(title: 'Accueil'));
        }

        if(settings.name == '/entrees'){
          return MaterialPageRoute(builder: (context) => EntreesPage(title: "Liste des recettes", etat: "entree"));
        }
        var uri=Uri.parse(settings.name ?? '');
        if(uri.pathSegments.length == 2 && uri.pathSegments.first == 'posts'){
          var id = int.parse(uri.pathSegments[1]);
          return MaterialPageRoute(builder: (context) => PostPage(id: id));
        }
      },
    );
  }
}
