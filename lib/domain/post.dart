class Post {
  late String id_recette;
  late String nom;
  late String descriptif;
  late String temps_preparation; //timestamp
  late String Nombre_de_personnes;
  late String Temps_cuisson;
  late String id_user;

// modèle du post
  Post(
        {
          required this.id_recette,
          required this.nom,
          required this.descriptif,
          required this.temps_preparation, //timestamp
          required this.Nombre_de_personnes,
          required this.Temps_cuisson,
          required this.id_user
        }
      );

  static Post fromJson(Map<String, dynamic> json) {
    return Post(
          id_recette:json["id_recette"],
          nom:json["nom"],
          descriptif:json["descriptif"],
          temps_preparation:json["temps_preparation"], //timestamp
          Nombre_de_personnes:json["Nombre_de_personnes"],
          Temps_cuisson:json["Temps_cuisson"],
          id_user:json["id_user"]
        );
  }

  Map<String, dynamic> toJson(){
    return{

      'id_recette': id_recette,
      'nom': nom,
      'descriptif': descriptif,
      'temps_preparation': temps_preparation,
      'Nombre_de_personnes': Nombre_de_personnes,
      'Temps_cuisson': Temps_cuisson,
      'id_user': id_user
  };
}
  static List<Post> fromList(Iterable list){
    return List<Post>.from(list.map((e)=>Post.fromJson(e)));
  }
}
